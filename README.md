
# Frontend EDUCALYTICS

Frontend desenvolvido para o Projeto API (Aprendizagem por projeto integrado) do 5º Semestre de Banco de dados.

Frontend desenvolvido com HTML, CSS, Javascript, React, Styled Components, Axios.

## Backend EDUCALYTICS

O repositório do backend (repositório principal) se encontra em [https://gitlab.com/rafaelEstevam/back-educalytics](https://gitlab.com/rafaelEstevam/back-educalytics).

## Aplicação React

Iniciar o projeto em REACT.

É obrigatório ter o Node JS instalado. Após a instalação, dentro da pasta do projeto, rodar os seguintes comandos:

```
npm install
```
- Instalar as dependências do projeto

```
npm start
```
- Rodar o projeto em ambiente local, com apontamento para backend também local.
