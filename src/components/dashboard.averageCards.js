import React from 'react';
import {Paper, CardHeader, Divider, CardContent, Typography, Tooltip} from '@material-ui/core';
import {getSatisfactionEmoji} from 'services/emoji';
import styled from 'styled-components';
import {Help} from '@material-ui/icons';

const CustomCardContent = styled(CardContent)`
    display: flex;
    align-items: center;
    flex-direction: column;
`

const AverageCard = ({title, subtitle, label, value, type = "percent", helper}) => {

    return (
        <Paper elevation={0}>
            <CardHeader
                title={title}
                subheader={subtitle}
                action={
                    helper && (<Tooltip title={helper}><Help /></Tooltip>)
                }
            />
            <Divider/>
            <CustomCardContent>
                <Typography variant="subtitle2">{label}</Typography>
                <Typography variant="h4">
                    {type === "percent" && ( value && getSatisfactionEmoji(value)?.emoji)}
                    {value} {type === "percent" && "%"} {type === "hour" && "minutos"}
                </Typography>
            </CustomCardContent>
        </Paper>
    )

}

export default AverageCard;