import React, { useState, useEffect } from 'react';
import { Paper, CardHeader, Divider, CardContent, TextField, Typography, Button } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import styled from 'styled-components';
import { useSnackbar } from 'notistack';
import { API } from 'services/api';

import GaugeChartComponent from './dashboard.gaugeChart';

import SearchIcon from '@material-ui/icons/Search';

const CardData = styled('div')`
	display: flex;
	justify-content: space-between;
	max-width: 250px;
`

const CardDataItem = styled('div')`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	text-align: center;
	padding: 0px 10px;
`

const PerformanceCard = ({ title, subtitle, label, value, type }) => {

	const { enqueueSnackbar } = useSnackbar();
	const [id, setId] = useState('');
	const [data, setData] = useState([]);
	const [performanceData, setPerformanceData] = useState({});
	const [students, setStudents] = useState([]);

	const handleReset = () => {
		setData([]);
		setPerformanceData({});
	}

	const handleGetData = async (e, newValue) => {

		const data = {
			student: {
				id: newValue?.id
			}
		}

		handleReset();
		try {
			const result = await API.post(`/factPerformance/filter`, data);
			let courses = [];
			result.data.map((item) => {
				courses = [...courses, ...[item]];
				return item;
			})
			setData(courses);
		} catch {
			enqueueSnackbar('Não foi possível buscar os dados', { variant: 'error' });
		}
	}

	const handleSelectCourse = (value) => {
		const course = data?.filter((item) => {
			return item.course.id == value
		});
		setPerformanceData(course[0]);
	}

	const handleGetAllStudent = async () => {
		try {
			const result = await API.get('/student/all');
			setStudents(result.data);
		} catch {
			enqueueSnackbar('Não foi possível buscar os dados', { variant: 'error' });
		}
	}

	useEffect(() => {
		handleGetAllStudent();
	}, [])

	return (
		<Paper elevation={0}>
			<CardHeader
				title={title}
				subheader={subtitle}
			/>
			<Divider />
			<CardContent>
				<div style={{ display: 'flex' }}>
					<Autocomplete
						id="combo-box-demo"
						options={students}
						getOptionLabel={(option) => option.name}
						onChange={(e, newValue) => handleGetData(e, newValue)}
						fullWidth
						renderInput={(params) => <TextField {...params}  fullWidth size="small" label="Nome do aluno" variant="outlined" />}
					/>
					{/* <TextField
						fullWidth
						label="Nº de matrícula"
						name="numberStudant"
						variant="outlined"
						size="small"
						placeholder="Informe a matrícula do aluno"
						value={id}
						onChange={(e) => {
							setId(e.target.value);
							handleReset();
						}}
					/>
					<Button variant="contained" color="primary" onClick={() => handleGetData()}>
						<SearchIcon />
					</Button> */}
				</div>
				<br />
				<TextField
					fullWidth
					size="small"
					label=""
					name=""
					variant="outlined"
					select
					SelectProps={{ native: true }}
					onChange={(e) => handleSelectCourse(e.target.value)}
				>
					<option>Selecione</option>
					{data?.map((item) => (
						<option key={item.course.id} value={item.course.id}>{item.course.name}</option>
					))}
				</TextField>

				<br />
				<br />
				<div>
					<Typography align="center" variant="subtitle2">
						{performanceData?.course?.name}
					</Typography>
					<GaugeChartComponent value={parseInt(performanceData?.course?.avaliacao) / 10} />
					<Typography align="center" variant="subtitle2">
						Nota parcial {performanceData?.course?.avaliacao}
					</Typography>
				</div>

				<br />
				<Divider />
				<br />

				{/* <div style={{ display: 'flex', justifyContent: 'center' }}>
					<CardData>
						<CardDataItem>
							<Typography variant="subtitle2">
								Aulas assistidas
							</Typography>
							<Typography variant="h4">
								{performanceData?.parClasses}
							</Typography>
						</CardDataItem>
						<CardDataItem>
							<Typography variant="subtitle2">
								Total de aulas
							</Typography>
							<Typography variant="h4">
								{performanceData?.totClasses}
							</Typography>
						</CardDataItem>
					</CardData>
				</div> */}

			</CardContent>
		</Paper>
	)

}

export default PerformanceCard;