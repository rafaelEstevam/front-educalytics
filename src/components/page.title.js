import React from 'react';

import { Grid, Typography, Divider } from '@material-ui/core';

const Title = ({ title, subtitle }) => {
    return (
        <Grid container spacing={5}>
            <Grid item xs={12}>
                <Typography variant="h5">
                    {title}
                </Typography>
                {subtitle && (
                    <Typography variant="subtitle1">
                        {subtitle}
                    </Typography>
                )}
                <br />
                <Divider />
            </Grid>
        </Grid>
    )
}

export default Title;