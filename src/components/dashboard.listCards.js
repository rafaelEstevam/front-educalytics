import React, {useEffect, useState} from 'react';
import {Paper, CardHeader, Divider, CardContent, Typography, Card, Tooltip} from '@material-ui/core';
import {getSatisfactionEmoji} from 'services/emoji';
import styled from 'styled-components';
import { API } from 'services/api';

const CustomCardContent = styled(CardContent)`
    display: flex;
    align-items: center;
    justify-content: space-between;
    div{
        width: 100%;
    }
`;

const DataTitle = styled(Typography)`
    background: #f5f5f5;
    padding: 10px;
    box-shadow: 0px 0px 4px rgba(0,0,0,0.2);
`;

function compare(items, flag) {
    items.sort(function (a, b) {
        if (a[flag] < b[flag]) {
          return 1;
        }
        if (a[flag] > b[flag]) {
          return -1;
        }
        return 0;
      });

    return items;
};

const mockBestStudents = [
    {id: 1, name: 'Rafael', averageAvailable: 8.5 },
    {id: 2, name: 'Carol', averageAvailable: 9.5 },
    {id: 3, name: 'Wilson', averageAvailable: 7 },
    {id: 4, name: 'Fábio', averageAvailable: 8.5 },
    {id: 5, name: 'Gabriela', averageAvailable: 6 },
    {id: 6, name: 'Felipe', averageAvailable: 9 },
    {id: 7, name: 'Alexandre', averageAvailable: 8.5 }
];

const mockBestEngagment = [
    {id: 1, name: 'Rafael', averageEngagment: 5},
    {id: 2, name: 'Carol', averageEngagment: 4},
    {id: 3, name: 'Wilson', averageEngagment: 8},
    {id: 4, name: 'Fábio', averageEngagment: 4},
    {id: 5, name: 'Gabriela', averageEngagment: 7},
    {id: 6, name: 'Felipe', averageEngagment: 3},
    {id: 7, name: 'Alexandre', averageEngagment: 2 }
];

compare(mockBestStudents, 'averageAvailable');
compare(mockBestEngagment, 'averageEngagment');

const ListCard = ({title, subtitle, label, value, type = "value"}) => {

    const [bestStudents, setBestStudents] = useState([]);
    const [bestEngagment, setBestEngagment] = useState([]);
    const [worstStudents, setWorstStudents] = useState([]);

    const handleGetFactPerformance = (order) => {
        API.get('/factPerformance/select', {params: {order}}).then((response) => {
            if(order === 'ASC'){
                setWorstStudents(response.data);
                console.log(response.data);
            }
            if(order === 'DESC'){
                setBestStudents(response.data);
            }
        })
    }

    const handleGetFactParticipation = () => {
        API.get('/factParticipation/select').then((response) => {
            setBestEngagment(response.data);
        })
    }

    useEffect(() => {
        handleGetFactPerformance('ASC');
        handleGetFactPerformance('DESC');
        handleGetFactParticipation();
    }, [])

    return (
        <Paper elevation={0}>
            <CardHeader
                title={title}
                subheader={subtitle}
            />
            <Divider/>
            <CustomCardContent>
                <div>
                    <Tooltip title="Por matéria">
                        <DataTitle align="center">Melhores alunos</DataTitle>
                    </Tooltip>
                    {bestStudents?.map((item) => (
                        <Card style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', padding: '10px', borderRadius: '0px'}}>
                            <Typography>{item.student.name}</Typography>
                            <Typography variant="subtitle2">{item.grade}</Typography>
                        </Card>
                    ))}
                </div>

                <div>
                    <Tooltip title="Por matéria">
                        <DataTitle align="center">Piores alunos</DataTitle>
                    </Tooltip>
                    {worstStudents?.map((item) => (
                        <Card style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', padding: '10px', borderRadius: '0px'}}>
                            <Typography>{item.student.name}</Typography>
                            <Typography variant="subtitle2">{item.grade}</Typography>
                        </Card>
                    ))}
                </div>

                <div>
                    <DataTitle align="center">Mais engajados</DataTitle>
                    {bestEngagment?.map((item) => (
                        <Card style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', padding: '10px', borderRadius: '0px'}}>
                            <Typography>{item.student.name}</Typography>
                            <Typography variant="subtitle2">{item.elap}</Typography>
                        </Card>
                    ))}
                </div>

            </CustomCardContent>
        </Paper>
    )

}

export default ListCard;