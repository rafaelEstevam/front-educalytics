import {
    Paper,
    CardContent, CardHeader, Grid, Tooltip, Typography
} from '@material-ui/core';
import React from 'react';

const PerformanceCard = ({ item }) => {

    return (
        <Grid item xs={2}>
            <Paper elevation={0}>
                <CardHeader
                    title={item.student.name}
                />
                <CardContent>
                    <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                        <div>
                            <Typography variant="subtitle2">Matéria:</Typography>
                            <Typography variant="h6">{item.course.name}</Typography>
                        </div>
                        <div>
                            <Typography variant="subtitle2">Nota:</Typography>
                            <Typography variant="h6" align="right">{item.course.avaliacao}</Typography>
                        </div>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <div>
                            <Typography variant="subtitle2">Ano:</Typography>
                            <Typography>{item.time.year}</Typography>
                        </div>
                        <div>
                            <Typography variant="subtitle2">Semestre:</Typography>
                            <Typography align="right">{item.time.semestre}</Typography>
                        </div>
                    </div>
                </CardContent>
            </Paper>
        </Grid>
    )
}

export default PerformanceCard;