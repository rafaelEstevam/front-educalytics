import React, { useEffect, useState } from 'react';
import {
	Paper,
	Card,
	CardHeader,
	TextField,
	Divider,
	CardContent,
	Grid,
	IconButton,
	Popover,
	Typography,
	Tooltip as Tool,
	Container,
	Chip,
	Button
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Chart, Line, Point, Tooltip } from "bizcharts";
import { useSnackbar } from 'notistack';
import moment from 'moment';

import { API } from 'services/api';

import styled from 'styled-components';

const FilterComponent = styled('div')`
	max-width: 250px;
`

const NumberOfAccessChart = ({ data }) => {
	return (
		<>
			<Chart
				appendPadding={[10, 0, 0, 10]}
				autoFit
				height={300}
				data={data}
				scale={{ value: { min: 0, alias: 'Nº de acessos', type: 'linear-strict' }, day: { range: [0, 1] } }}
			>
				<Line position="day*value" />
				<Point position="day*value" />
				<Tooltip showCrosshairs />
			</Chart>
		</>
	);
}

const format = (result) => {
	result.data.map((item) => {
		item.day = `${moment(new Date(item.actSemYear)).add(3, 'hours').format('YYYY')} (${item.actSemestre})`;
		item.value = item.actNumUsr;
		return item;
	})
	return result.data;
}

const labelTranslate = (item) => {
	return item === "number" ? "Nº Acessos >= a" : item === "semester" ? "Semestre" : "A partir de "
}

const NumberOfAccess = () => {

	const { enqueueSnackbar } = useSnackbar();
	const [data, setData] = useState();
	const [anchorEl, setAnchorEl] = React.useState(null);
	const [filterForm, setFilterForm] = useState({});
	const [filterTags, setFilterTags] = useState({});

	const handleClick = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const open = Boolean(anchorEl);
	const id = open ? 'simple-popover' : undefined;

	const handleGetData = async () => {
		try {
			const result = await API.get('/activation/all');
			format(result);

			setData(format(result));
		} catch {
			enqueueSnackbar('Não foi possível trazer os dados', { variant: 'error' });
		}
	}

	const handleChange = (event) => {
		setFilterForm({
			...filterForm,
			[event.target.name]: event.target.value
		});
	};

	const handleOnSubmitFilter = async (e) => {
		e.preventDefault();
		setFilterTags(filterForm);
		const data = {
			usr: filterForm.number && parseInt(filterForm.number, 10),
			sem: filterForm.semester && parseInt(filterForm.semester, 10),
			year: filterForm.year && `${filterForm.year}-01-01`
		}
		try {
			const result = await API.post('/activation/filter', data);
			setData(format(result));
			handleClose();
		} catch {
			enqueueSnackbar('Não foi possível trazer os dados', { variant: 'error' });
		}

	}

	useEffect(() => {
		handleGetData();
	}, []);

	return (
		<>
			<Paper elevation={0}>
				<CardHeader
					title="Nº de usuários do sistema"
					subheader="Por semestre"
					action={
						<Tool title="Filtro">
							<IconButton aria-label="settings" aria-describedby={id} onClick={handleClick}>
								<MoreVertIcon />
							</IconButton>
						</Tool>
					}
				/>
				<div style={{ padding: '0px 15px 15px' }}>
					{Object.keys(filterTags)?.map((item) => (
						<>
							{filterTags[item] && (
								<Chip style={{ marginRight: '10px' }} variant="outlined" label={`${labelTranslate(item)} ${filterTags[item]}`} />
							)}
						</>
					))}
				</div>

				<Divider />
				<CardContent>
					<NumberOfAccessChart data={data} />
				</CardContent>
			</Paper>
			<Popover
				id={id}
				open={open}
				anchorEl={anchorEl}
				onClose={handleClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'center',
				}}
				transformOrigin={{
					vertical: 'center',
					horizontal: 'center',
				}}
			>
				<CardContent>
					<Typography >
						Filtro
					</Typography>
				</CardContent>

				<Divider />
				<CardContent>
					<FilterComponent>
						<form onSubmit={(e) => handleOnSubmitFilter(e)}>
							<Grid container spacing={2}>
								<Grid item xs={6}>
									<TextField
										fullWidth
										size="small"
										label="Nº de acessos"
										name="number"
										type="number"
										variant="outlined"
										onChange={handleChange}
										value={filterForm.number}
										InputLabelProps={{
											shrink: true,
										}}
									/>
								</Grid>
								<Grid item xs={6}>
									<TextField
										fullWidth
										size="small"
										label="Semestre"
										name="semester"
										type="number"
										InputProps={{ inputProps: { min: 1, max: 2 } }}
										max="2"
										variant="outlined"
										onChange={handleChange}
										value={filterForm.semester}
										InputLabelProps={{
											shrink: true,
										}}
									/>
								</Grid>
								<Grid item xs={12}>
									<TextField
										fullWidth
										size="small"
										label="Ano"
										name="year"
										type="number"
										InputProps={{ inputProps: { min: 2000, max: new Date().getFullYear() } }}
										variant="outlined"
										onChange={handleChange}
										value={filterForm.year}
										InputLabelProps={{
											shrink: true,
										}}
									/>
								</Grid>
								<Grid item xs={12}>
									<Button
										fullwidth
										size="small"
										variant="outlined"
										type="submit"
										color="primary"
									>
										Aplicar filtro
									</Button>
								</Grid>
							</Grid>
						</form>
					</FilterComponent>
				</CardContent>
			</Popover>
		</>
	)
}

export default NumberOfAccess;