import React from 'react';

import { render, screen } from '@testing-library/react'

import PageTitle from '../../components/page.title';

describe('<PageTitle />', () => {
  it('should render the heading', () => {
    const { container } = render(<PageTitle title="react avançado" />)
    expect(
      screen.getByRole('heading', { name: /react avançado/i })
    ).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
