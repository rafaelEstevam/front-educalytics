import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import {getTokenInStorage, decodeToken} from 'services/api'

import Dashboard from './pages/Dashboard'
import Performance from './pages/Performance'
import Login from './pages/Login'
import Profile from './pages/Profile'
import UserList from './pages/UserList'
import DefaultLayout from './templates/default'

const AdminRoutes = ({ component: Component, auth,  ...attrs }) => {

  const token = getTokenInStorage();
  const permission = decodeToken(token).permission === auth;

  // const token = "asdfkj";
  // const permission = "ROLE_ADMIN";

  return token && permission ? (
    <Route
      {...attrs}
      render={(props) => (
        <DefaultLayout>
          <Component {...props} />
        </DefaultLayout>
      )}
    />
  ) : (
    <Redirect to="/404" />
  )
}

const PrivateRoute = ({ component: Component, auth,  ...attrs }) => {

  const token = getTokenInStorage();
  // const token = true;
  
  return token ? (
    <Route
      {...attrs}
      render={(props) => (
        <DefaultLayout>
          <Component {...props} />
        </DefaultLayout>
      )}
    />
  ) : (
    <Redirect to="/404" />
  )
}

function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Login} />
      <PrivateRoute path="/dashboard" component={Dashboard} />
      <PrivateRoute path="/performance" component={Performance} />
      <AdminRoutes path="/users" auth="ROLE_ADMIN" component={UserList} />
      <PrivateRoute path="/profile/edit" component={Profile} />
      <Route path="*" component={() => <h1> 404 </h1>} />
    </Switch>
  );
}

export default Routes;