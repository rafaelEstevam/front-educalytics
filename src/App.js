import react, { useEffect, useState } from 'react';
import { Router } from 'react-router-dom';
import GlobalStyle from './styles/global';

import { createTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import Routes from './routes';
import history from './services/history';

import DefaultContext from './stores/defaultContext';

const theme = createTheme({
  palette: {
    primary: {
      main: "#292961",
    },
    secondary: {
      main: '#ff5c5c',
    }
  },
  typography: {
    fontSize: 12,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  overrides: {
    MuiButton: {
      text: {
        color: 'white',
      },
    },
  },
});

function App() {

  const [defaultContext, setDefaultContext] = useState({});

  return (
    <DefaultContext.Provider value={defaultContext}>
      <ThemeProvider theme={theme}>
        <Router history={history}>
          <Routes />
          <GlobalStyle />
        </Router>
      </ThemeProvider>
    </DefaultContext.Provider>

  );
}

export default App;
