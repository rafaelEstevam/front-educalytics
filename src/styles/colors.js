const primary = "#3f51b5" 
const secondary = "#f50057"
const danger = "rgba(242, 38, 19, 0.9)"
const warning = "rgba(248, 148, 6, 0.9)"
const success = "rgba(63, 195, 128, 0.9)"
const info = "#8137f7e6"

export const COLORS = {
    primary,
    secondary,
    danger,
    warning,
    success,
    info
}