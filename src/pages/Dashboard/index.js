import {
  Grid
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { API } from 'services/api';
import { useSnackbar } from 'notistack';

import Title from 'components/page.title';
import NumberOfAccess from 'components/dashboard.numberOfAccess';
import AverageCard from 'components/dashboard.averageCards';
import ListCard from 'components/dashboard.listCards';
import PerformanceCard from 'components/dashboard.performance';

function Dashboard() {

  const {enqueueSnackbar} = useSnackbar();
  const [data, setData] = useState({});
  const [mongoData, setMongoData] = useState({});

  const handleGetData = async() => {
    try{
      const result = await API.get('/tss/avgData');
      setData(result.data);
    }catch{
      enqueueSnackbar('Não foi possível buscar os dados', {variant: 'error'});
    }
  }

  const handleGetMongoData = async() => {
    try{
      const result = await axios.get('https://api-chat-educalytics.herokuapp.com/connections/data/all');
      setMongoData(result.data);
    }catch{
      enqueueSnackbar('Não foi possível buscar os dados', {variant: 'error'});
    }
  }

  useEffect(() => {
    handleGetData();
    handleGetMongoData();
  }, []);

  return (
    <>
      <Grid container spacing={5}>
        <Grid item xs={9}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Title title="Dashboard" />
            </Grid>
            <Grid item xs={7}>
              <Grid container spacing={3}>
                <Grid item xs={4}>
                  <AverageCard
                    helper="Em minutos"
                    title="Tempo Total"
                    subtitle="Uso da plataforma"
                    type="hour"
                    label="Alunos"
                    value={mongoData?.totalTimeOnlineInMinutes}
                  />
                </Grid>
                <Grid item xs={4}>
                  <AverageCard
                    title="Total de conexões"
                    subtitle="Logins na plataforma"
                    label="Alunos"
                    type="value"
                    value={mongoData?.totalConnections}
                  />
                </Grid>
                <Grid item xs={4}>
                  <AverageCard
                    helper="Total de minutos online dos usuários / Total de minutos ao longo do ano"
                    title="Engajamento médio"
                    subtitle="Uso da plataforma"
                    label="Alunos"
                    value={(mongoData?.averageEngagementForConnection * 100).toFixed(2)}
                  />
                </Grid>
              </Grid>
              <br/>
              <NumberOfAccess />
            </Grid>
            <Grid item xs={5}>
              <ListCard title="Média / Engajamento" subtitle="" label="Alunos" />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={3}>
          <PerformanceCard title="Avaliação de desempenho" />
        </Grid>
      </Grid>
    </>
  );
}

export default Dashboard;