import {
  Paper,
  CardContent, Grid, TextField,
  Typography,
  LinearProgress
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import PerformanceCard from 'components/performanceCard.component';
import Title from 'components/page.title';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { API } from 'services/api';


function Performance() {

  const { enqueueSnackbar } = useSnackbar();
  const [courses, setCourses] = useState({});
  const [students, setStudents] = useState({});
  const [course, setCourse] = useState({});
  const [student, setStudent] = useState({});
  const [year, setYear] = useState('');
  const [semester, setSemester] = useState('');
  const [result, setResult] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleGetData = async () => {
    try {
      const coursesResult = await API.get('/crs/all');
      const studentsResult = await API.get('/student/all');

      setCourses(coursesResult.data);
      setStudents(studentsResult.data);

    } catch {
      enqueueSnackbar('Não foi possível buscar os dados', { variant: 'error' });
    }
  }

  const submitFilter = async () => {

    setLoading(true);

    const filter = {};

    if (student?.id) {
      filter.student = {
        id: student.id
      }
    }

    if (course?.id) {
      filter.course = {
        id: course.id
      }
    }

    if (year) {
      filter.time = {
        year: year,
        semestre: semester
      }
    }

    try {
      const result = await API.post('/factPerformance/filter', filter);
      setLoading(false);
      setResult(result.data);
    } catch {
      enqueueSnackbar('Não foi possível buscar os dados', { variant: 'error' });
      setLoading(false);
    }

  }

  useEffect(() => {
    submitFilter();
  }, [semester]);

  useEffect(() => {
    submitFilter();
  }, [year]);

  useEffect(() => {
    submitFilter();
  }, [student])

  useEffect(() => {
    submitFilter();
  }, [course]);

  useEffect(() => {
    handleGetData();
  }, []);

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Title title="Performance" subtitle="Todos os dados de avaliação na plataforma" />
        </Grid>
        <Grid item xs={12}>
          <Paper elevation={0}>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md={2}>
                  <Typography align="center">
                    Filtrar por:
                  </Typography>
                </Grid>
                <Grid item xs={12} md={10}>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Autocomplete
                        id="combo-box-demo"
                        options={students}
                        getOptionLabel={(option) => option.name}
                        onChange={(e, newValue) => {
                          setStudent(newValue);
                        }}
                        fullWidth
                        renderInput={(params) => <TextField {...params} fullWidth size="small" label="Nome do aluno" variant="outlined" />}
                      />
                    </Grid>
                    <Grid item xs={2}>
                      <Autocomplete
                        id="combo-box-demo"
                        options={courses}
                        getOptionLabel={(option) => option.name}
                        onChange={(e, newValue) => {
                          setCourse(newValue);
                        }}
                        fullWidth
                        renderInput={(params) => <TextField {...params} fullWidth size="small" label="Curso" variant="outlined" />}
                      />
                    </Grid>
                    <Grid item xs={2}>
                      <TextField
                        fullWidth
                        size="small"
                        label="Ano"
                        type="number"
                        name="year"
                        variant="outlined"
                        value={year}
                        onChange={(e) => {
                          setYear(e.target.value)
                        }}
                      />
                    </Grid>
                    <Grid item xs={2}>
                      <TextField
                        fullWidth
                        size="small"
                        label="Semestre"
                        type="number"
                        name="semester"
                        variant="outlined"
                        disabled={!year}
                        value={semester}
                        onChange={(e) => {
                          setSemester(e.target.value)
                        }}
                      />
                    </Grid>
                    {/* <Grid item xs={2}>
                      <Button fullWidth variant="outlined" color="primary" onClick={() => submitFilter()}>
                        Buscar
                      </Button>
                    </Grid> */}
                  </Grid>
                </Grid>
              </Grid>
            </CardContent>
          </Paper>
          {loading && (
            <LinearProgress />
          )}
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={2}>
            {result.length > 0 ? (
              <>
                {result?.map((item) => (
                  <PerformanceCard item={item} />
                ))}
              </>
            ) : (
              <Grid item xs={12}>
                <Typography align="center">
                  Nenhum dado encontrado com esses parâmetros
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}

export default Performance;